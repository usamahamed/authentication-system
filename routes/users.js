var express = require('express');
var router = express.Router();
var User=require('../modules/User')
var passport=require('passport');
var LocalStrategy=require('passport-local').Strategy;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('users', { title: 'users' });
});

router.get('/register', function(req, res, next) {
  res.render('Register', { title: 'register' });
});
router.get('/login', function(req, res, next) {
  res.render('login', { title: 'login' });
});

router.post('/register', function(req, res, next) {
  //get values
  	var name=req.body.name;
	var email=req.body.email;
	var username=req.body.username;
	var password=req.body.password;
	var password1=req.body.password1;

//check for images
if(req.body.profileimg){
	console.log('uploading image....');
	//file info
	var profileImageOrignalName  =req.files.profileimg.originalname;
	var profileImageName         =req.files.profileimg.name;
	var profileImagemime         =req.files.profileimg.mimetype;
	var profileImagepath         =req.files.profileimg.path;
	var profileImageExt         =req.files.profileimg.extention;
	var profileImagesize         =req.files.profileimg.size;

}else{
	//set default image
		var profileImageName="noimage.png";
}

//form validation
req.checkBody('name','name is required').notEmpty();
req.checkBody('email','email is required').notEmpty();
req.checkBody('email','not email').isEmail();
req.checkBody('username','username is required').notEmpty();
req.checkBody('password','password is required').notEmpty();
req.checkBody('password1','password dont match').equals(req.body.password);
//check for errors
var errors=req.validationErrors();
if(errors){
	res.render('register',{
		errors:errors,
		name:name,
		email:email,
		username:username,
		password:password,
		password1:password1

	});
}else{
	var newUser = new User({
		username:username,
		password:password,
		email:email,
		name:name,
		profileimg:profileImageName
	});
	//create user
	User.createUser(newUser,function(err,user){
		if(err) throw err;
		console.log(user);
	});
	//success message
	req.flash('you now register you may login');
	res.location('/');
	res.redirect('/');
}

});


passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});

passport.use(new LocalStrategy(
  function(username, password, done) {
  	User.getUserByUsername(username, function (err, user) {
      if (err) { return done(err); }
	  if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      User.comparePassword(password,user.password,function(err,isMatch){
		if (err) throw err;
		if(isMatch){
			return done(null,user);
		}else	   {
			console.log('Invalid password');
			return done(null,false,{message:'Invalid password'});
			        }	

      });
  	});

}
	));

router.post('/login',passport.authenticate('local',{failureRedirect:'/users/login/',failureFlash: 'Invalid username or password.'}),function(req,res){
console.log('authentication success');
req.flash('success','you are login');
res.redirect('/');
});

router.get('/logout',function(req,res){

	req.logout();
	req.flash('success','you logout');
	res.redirect('/users/login');
});
module.exports = router;
