var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/',ensureAuthinticated, function(req, res, next) {
  res.render('index', { title: 'members' });
});
function ensureAuthinticated(req,res,next){
	if(req.isAuthenticated()){
		return next();
	}
	res.redirect('users/login');
}
module.exports = router;
