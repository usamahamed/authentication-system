var mangoose=require('mongoose');
var bcrypt=require('bcrypt');
mangoose.connect('mongodb://localhost:27017/nodeauth');
var db=mangoose.connection;

//define schema
var UserSchema = mangoose.Schema({
	username:{
		type : String,
		index: true
	},
password:{
		type : String ,required:true ,bcrypt:true
	},
	email:{
		type : String
	},
	name:{
		type : String
	},
	profileimg:{
		type : String
	}
});
var User=module.exports =mangoose.model('User' , UserSchema);

module.exports.comparePassword = function(candidatePassword,hash,callback){
bcrypt.compare(candidatePassword,hash,function(err,isMatch){
if(err) return callback(err);
callback(null,isMatch);
});

}


module.exports.getUserByUsername = function(username,callback){
	var quairy={username:username};
	User.findOne(quairy,callback);

}
module.exports.getUserById = function(id,callback){
	User.findById(id,callback);

}
module.exports.createUser = function(newUser,callback){
	bcrypt.hash(newUser.password,10,function(err,hash){
		if (err) throw err;
		//set hashed
		newUser.password=hash;
		//creat user
			newUser.save(callback);

	});
}